# frozen_string_literal: true

RSpec.describe Strum::Service do
  it "has a version number" do
    expect(Strum::Service::VERSION).not_to be nil
  end

  let(:empty_service_class) do
    Class.new do
      include Strum::Service
      define_method(:call) {} # rubocop:disable Lint/EmptyBlock
    end
  end

  it "empty result" do
    expect(empty_service_class.call({})).to eq(nil)
  end
end
