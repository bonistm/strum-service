# frozen_string_literal: true

require "strum/service/version"

module Strum
  # Strum service
  module Service # rubocop:disable Metrics/ModuleLength
    class Failure < StandardError; end

    def self.included(base)
      base.class_eval do
        extend ClassMethods
      end
    end

    # Define coercive methods
    COERCIVE_METHODS = %i[add_error add_errors any required sliced sliced_list].freeze

    COERCIVE_METHODS.each do |m|
      define_method("#{m}!") do |*args, **kwargs, &block|
        send(m, *args, **kwargs, &block)
        throw :exit unless valid?
      end
    end

    # Internal: Interactor class methods.
    module ClassMethods
      def call(main_input, **args, &block)
        new(main_input, **args).execute(&block)
      end
    end

    # Instance methods
    def initialize(main_input, **args)
      self.strum_errors = {}
      self.service_handlers = { on: {}, success: {}, failure: {} }
      self.outputs = {}
      self.inputs = args.merge(default: main_input)
      self._inputs = inputs.dup.freeze
      setup_default_proc
      setup_input_default_proc
    end

    def execute
      catch(:exit) do
        audit
        yield(self) if block_given?
        call if valid?
      end
      valid? ? valid_result : invalid_result
    end

    def hook(name, data = self)
      service_handlers[:on][name].is_a?(Proc) && service_handlers[:on][name].call(data)
    end

    def valid?
      strum_errors.empty?
    end

    def errors
      strum_errors
    end

    def on(arg, &block)
      service_handlers[:on][arg] = block
    end

    def success(arg = nil, &block)
      service_handlers[:success][arg] = block
    end

    def failure(arg = nil, &block)
      service_handlers[:failure][arg] = block
    end

    def method_missing(method_name, *args, &_block)
      (args.count.zero? &&
        (input.is_a?(Hash) && input[method_name.to_sym]) ||
        inputs[method_name.to_sym]) ||
        super
    end

    def respond_to_missing?(method_name, include_private = false)
      input.is_a?(Hash) && input[method_name.to_sym] || super
    end

    protected

      attr_accessor :inputs, :_inputs, :strum_errors, :outputs, :service_handlers

      def call
        raise Failure, "call method must be implemented"
      end

      def audit; end

      def input
        inputs[:default]
      end

      def input=(value)
        inputs[:default] = value
      end

      def _input
        _inputs[:default]
      end

      def args
        inputs.slice(*inputs.keys - [:default])
      end

      def output_value(key = :default)
        @outputs[key]
      end

      # rubocop: disable Style/OptionalArguments
      def output(key = :default, value)
        @outputs[key] = value
      end
      # rubocop: enable Style/OptionalArguments

      # def add_error(field, value)
      #   if field.is_a?(Array)
      #     field.each do |key|
      #       value.is_a?(Array) ? value.each { |v| add_error(key, v) } : add_error(key, value)
      #       add_error(key, value.is_a?(Array) ? value.first : value)
      #     end
      #   else
      #     strum_errors[field] ||= []
      #     strum_errors[field] = strum_errors[field] + Array(value)
      #   end
      # end

      def add_error(field, value)
        strum_errors[field] ||= []
        strum_errors[field] += Array(value)
      end

      def add_errors(errors)
        errors.each { |field, value| add_error(field, value) }
      end

      def required(*keys)
        if input.is_a?(Hash)
          (keys - input.keys).each { |key| add_error(key, :field_must_exist) }
        else
          add_error(:input, :must_be_hash)
        end
      end

      def any(*keys)
        if input.is_a?(Hash)
          add_error(:input, :any_field_must_exist) unless (keys & input.keys).count.positive?
        else
          add_error(:input, :must_be_hash)
        end
      end

      def sliced(*keys)
        if input.is_a?(Hash)
          self.input = input.slice(*keys)
        else
          add_error(:input, :must_be_hash)
        end
      end

      def sliced_list(*keys)
        add_error(:input, :must_be_array) && retuen unless input.is_a?(Array)

        self.input = input.map do |item|
          if item.is_a?(Hash)
            item.slice(*keys)
          else
            add_error(:input_subitem, :must_be_hash)
          end
        end
      end

      def array!
        self.input = [*input]
      end

    private

      def setup_default_proc
        inputs.default_proc = proc { |h, k| h.key?(k.to_s) ? h[k.to_s] : nil }
      end

      def setup_input_default_proc
        input.default_proc = proc { |h, k| h.key?(k.to_s) ? h[k.to_s] : nil } if input.is_a?(Hash)
      end

      def key_to_sym(key)
        key.to_sym
      rescue StandardError
        key
      end

      def handler_key
        ((outputs.keys << nil) & service_handlers[:success].keys).first
      end

      def handler
        service_handlers[:success][handler_key]
      end

      def valid_result
        if handler.is_a?(Proc)
          handler.call(outputs[handler_key] || outputs[:default])
        else
          outputs[handler_key] || outputs[:default]
        end
      end

      def invalid_result
        handler = service_handlers[:failure][((errors.values.flatten << nil) & service_handlers[:failure].keys).first]
        handler.call(errors) if handler.is_a?(Proc)
      end
  end
end
